import React, { Component } from 'react';

class Count extends Component {
	render(){
		return <h1>Count is {this.props.count}</h1>;
	}
}

export default Count;