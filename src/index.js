import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Hello from './Hello';
import Form from './Form';
import Count from './Count';

class App extends React.Component{

	constructor(){
		super()
		this.state = {
			name: '',
			count: 0
		};
		this.handleClick = this.handleclick.bind(this);
	}

	handleclick(){
		this.setState({count: ++this.state.count});
		console.log(this.state.count);
	}

	_updateName = event => {
		console.log("inside event ");
		const value = event.target.value;
		this.setState({name: value});
	}

    render() {
    	return(
    			<div className = 'button__container'>
	    			<Hello name = {this.state.name}/>
    				<Form updateNameHandler = {this._updateName}/>
    				<Count count = {this.state.count}/>
    				<button className='button' onClick={this.handleClick}>Click Me</button>
    			</div>
    		);
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));